import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';
import { db } from './src/config';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
      arrowLine: 2,
    };
  }

  componentDidMount() {
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 5000, maximumAge: 0, distanceFilter: 1 },

    );
    this.writeToFirebase(this.state.arrowLine, this.state.longitude, this.state.latitude);
  }

  componentDidUpdate() {
    this.updateFirebase(this.state.longitude, this.state.latitude);
  }

  writeToFirebase(currentLine, longitude, latitude) {
    db.ref('Trips').set({
      currentLine,
      longitude,
      latitude,
    }).then((data) => {
      //success callback
      console.log('data', data)
    }).catch((error) => {
      //error callback
      console.log('error', error)
    });
  }

  updateFirebase(longitude, latitude) {
    db.ref().update({
      longitude,
      latitude
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
    db.ref('Trips').remove();
  }

  render() {
    return (
      <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Latitude:: {this.state.latitude}</Text>
        <Text>Longitude:: {this.state.longitude}</Text>
        {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
});
